# IoT Project

## Hardware

* ESP32
* DHT22
* SSD1306

## Software

* MQTT protocol
* Broker Mosquitto
* SQLite DataBase
* Flask

## References
* https://mqtt.org/
* https://mosquitto.org/
* https://pypi.org/project/paho-mqtt/
* https://dataset.readthedocs.io/en/latest/
* https://inloop.github.io/sqlite-viewer/
* https://flask.palletsprojects.com/en/3.0.x/

## Objectives
1º - Send realtime temperature and humidity to clients in Python

2º - Adiciona a uma DP

3º - Mostrar o Gráfico

4ª - API que expõe os dados 

* uma melhoria seria usar o Redis para assegurar persistência. Tewr uma thread que escreve para redis e outra que escreve para a DB.
* para o UI atualizamos  do Redis para o historico lemos da DB. 
* melhoria usar o graffana para mostrar o gráfico -> API (vs plotly -> dashboard). O grafana liga-se à API

## Virtual Environment

__create__

    python -m venv venv

__delete__

    delete folder `venv`

__install__

    pip install -r requirements.txt

__append packages to `requirements.txt`__

    pip freeze > requirements.txt   

__execute__

    python mqtt_client.py

## Local operations
###Para fazer o load. estamos a meter o nome para poder fazer upload para o dockerhub. No caso do nexus pode ser importante.

__build__

    docker build -t kumandus/iot .

__run__

    docker run -d --name my-iot -p 80:5000 --link redis -v data:/src/db kumandus/iot 



__ver logs__

    docker logs my-iot -f

 __dependecy__

Redis

    docker run -d --name redis -p 6379:6379 -v redis-data:/data redis:alpine

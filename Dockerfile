# imagem base
FROM python:3.11-alpine

# porto
EXPOSE 5000

# copia para uma diretoria, podia ser uma qualquer ou não ter
WORKDIR /src

# copia requirements.txt para requirements.txt 
# foi feito primeiro porque o primeiro snapshoot contem ja o ambiente. e assim quando faco introduao de alteracoes ao meu código e nao tenho da fazer a recompliação total da imagem
COPY requirements.txt .

# install dependências
RUN pip install -r requirements.txt

# copy all
COPY . .

# run -> colocamos uma lista de lementos para não haver problemas de interpretacao pelo docker RUN
CMD ["python", "-m", "iot"]


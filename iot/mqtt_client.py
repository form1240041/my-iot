import datetime as dt

import paho.mqtt.client as mqtt
import dataset
import redis

try:
    from iot import settings
except ModuleNotFoundError:
    import settings

# redis connection
# cache = redis.Redis('redis', port=6379)
cache = redis.Redis(host=settings.REDIS_URL, port=6379)

def on_connect_callback(client, user_data, flags, rc):
    print(rc)
    if rc == 0:  # ok
        print('Connection OK')

        # subscreve na arvore MQTT tudo do sensors para a frente
        topic = 'esp32/+/sensors/#'
        client.subscribe(topic)
        client.message_callback_add(topic, on_message_callback)
    else:
        print('Connection Fail')


def on_message_callback(client, userdata, msg):
    # print(msg.topic, msg.payload)
    now = dt.datetime.now()
    topic = msg.topic.split('/')[-1]
    payload = float(msg.payload)
    '''
    print(dt.date.today(), topic[-1], float(msg.payload), sep=' : ')
    print(dt.datetime.now(), topic[-1], float(msg.payload), sep=' : ')
    print(f'{dt.datetime.now()} : { topic[-1] } : { float(msg.payload):.2f}') 
    '''

    data = {'timestamp': now, 'value': payload}

    #update redis
    cache.set(topic, str(data))

    db = dataset.connect(settings.DATABASE)
    print(f'{now} : {topic:<12} : {payload:.2f}')
    table = db[topic]
    table.insert(data)
    db.close()


def main():

    Client = mqtt.Client(client_id='RN-laptop')
    Client.on_connect = on_connect_callback  # pointer to function
    Client.username_pw_set('cfreire', '65Zc2E')
    Client.connect('iot.cfreire.pt')  # url of mosquitto broker
    Client.loop_forever()

# para correr local uma que o codigo foi colocado no main()
if __name__ == '__main__':
    main()
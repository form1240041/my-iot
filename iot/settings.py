# podia ser um ficheiro ini, mas assim podemos ter a opção de ser validado antes de chegarmos a runtime
__version__ = '1.1.1'

APP_NAME = 'iot'

MIN_PYTHON_VERSION = (3, 10)

DEFAULT_LOG_LEVEL = 'INFO'

DATABASE = 'sqlite:///db/sensors-data.sqlite'

REDIS_URL = 'redis'
# REDIS_URL = 'localhost'

REDIS_PORT = 6379
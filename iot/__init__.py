import sys
import logging

from iot import settings as sets

# nunca sera alvo de play

if sys.version_info[:2] < sets.MIN_PYTHON_VERSION:
    print("Error: minimum python version required:",
          sets.MIN_PYTHON_VERSION[0], '.', sets.MIN_PYTHON_VERSION[1],
          sep='')
    sys.exit(1)  # call ao sys é diferente de Exist(1)

#  Create logger 
#  grava para ficheiro
# logging.basicConfig(filename='my_sum.log', level=logging.INFO)
# nos dockes vimos o logo diretamente sem ser no ficheiro
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
logger.info(f'Starting {sets.APP_NAME} version V{sets.__version__}')
logger.setLevel(sets.DEFAULT_LOG_LEVEL)

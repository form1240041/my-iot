#!/usr/bin/python
# Controler
import datetime
import logging
from flask import Flask, render_template, jsonify, request
from waitress import serve
import redis


# view
import dataset

try:
    from iot import settings
except ModuleNotFoundError:
    import settings


logger = logging.getLogger(__name__)
app = Flask(__name__)
# cache = redis.Redis(host=settings.REDIS_URL, port=settings.REDIS_PORT)
cache = redis.Redis(host=settings.REDIS_URL, port=6379)

@app.route('/')  # decorator
def hello_world():
    return render_template('index.html', version=settings.__version__)


@app.route('/realtime')
def realtime():
    logger.info('Realtime function call')
    '''
    db = dataset.connect(settings.DATABASE)
    result = db.query('SELECT timestamp, value FROM temperature '
                      'ORDER BY timestamp DESC LIMIT 1')

    data = [row for row in result]
    db.close()  # só fecha depois de tirar os dados uma vez que o result é um iterador
    '''
    data = eval(cache.get('temperature'))
    logger.debug(data)
    return jsonify(data)

# devia de haver uma camada de buisness logic e não misturado na view


@app.route('/history')
def history():

    try:
        lim = int(request.args.get('limit'))
    except (TypeError, ValueError):
        lim = 10

    logger.info('History function call')
    db = dataset.connect(settings.DATABASE)  # TODO Create Controller function
    result = db.query('SELECT timestamp, value FROM temperature '
                      f'ORDER BY timestamp DESC LIMIT {lim}')

    data = [row for row in result]
    db.close()  # só fecha depois de tirar os dados uma vez que o result é um iterador
    logger.debug(*data)
    return jsonify(*data)


def main():
    logger.info(f'Starting IoT version {settings.__version__}')
    serve(app, host='0.0.0.0', port=5000, threads=5)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
